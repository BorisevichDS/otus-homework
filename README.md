# OTUS Homework

В репозитории хранятся проекты:

<b>ConsoleApplication</b>: Программа реализует процесс оценки разработчика. В программе использутся классы Developer (описывает сущность разработчик) и Rating (описывает рейтинг разработчик). В классе рейтинг реализован оператор сложения с одной перегрузкой. Демонстрация работы класса в методе Main.

<b>LinkedCircleList</b>: Программа демонстрирует работу кольцевого односвязанного списка. В рамках домашнего задания был реализован класс Node и LinkedCircleList, опирающийся на класс Node. Так же был определен и реализован интерфейс IAlgorithm. 

<b>LINQ</b>: Программа демонстрирует работу LINQ to Object запросов.

<b>RegExp</b>: Программа парсит страницу сайта habr.com и выводит в консоль список всех ссылок найденных в коде страницы сайта.

<b>Reflection</b>: Программа демонстрирует работу рефлексии. Так же в программе проводится замер времени работы сериализации/десериализации на основе рефлексии и с использованием библиотеки NewtonsoftJSON.dll.