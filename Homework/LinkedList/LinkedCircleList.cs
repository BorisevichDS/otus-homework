﻿using System;
using System.Collections;

namespace LinkedList
{
    interface IAlgorithm<T>
    {
        void Sort();
        int BinarySearch(T searchedValue);
    }
    
    /// <summary>
    /// Класс описывает кольцевой односвязанный список.
    /// </summary>
    class LinkedCircleList<T> : IEnumerable, IAlgorithm<T> where T: IComparable
    {
        private Node<T> _head;
        private Node<T> _tail;
        private bool _isSorted = false;

        /// <summary>
        /// Количество элементов в списке. 
        /// </summary>
        public int Count { get; private set; }

        public Node<T> this[int index]
        {
            get
            {
                if (index > Count - 1)
                {
                    throw new IndexOutOfRangeException();
                }
                else
                {
                    Node<T> current = _head;
                    for (int i = 1; i <= index; i++)
                    {
                        current = current.Next;
                    }
                    return current;
                }
            }
         }

        /// <summary>
        /// Метод преобразует список в массив элементов.
        /// </summary>
        /// <returns></returns>
        private Node<T>[] ToArray()
        {
            Node<T>[] mas = new Node<T>[Count];
            Node<T> current = _head;

            for (int i = 0; i < Count; i++)
            {
                mas[i] = current;
                current = current.Next;
            }
            return mas;
        }

        /// <summary>
        /// Метод добавляет элемент в список
        /// </summary>
        /// <param name="data"></param>
        public void Add(T data)
        {
            Node<T> node = new Node<T>(data);

            if (_head == null)
            {
                // Для первого элемента в списке голова и хвост смотрят на этот элемент
                _head = node;
                _tail = node;
                _tail.Next = _head;
            }
            else
            {
                // У предыдущего элемента меняем ссылку на следующий элемент списка
                _tail.Next = node;

                // Заменяем хвостовой элемент с предыдущего на текущий
                _tail = node;
                _tail.Next = _head;
            }
            Count++;
        }

        /// <summary>
        /// Метод очищает список
        /// </summary>
        public void Clear()
        {
            _head = null;
            _tail = null;
            Count = 0;
        }

        /// <summary>
        /// Метод проверяет наличие элемента в списке
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Contains(T data)
        {
            Node<T> current = _head;
            for (int i = 0; i < Count; i++)
            {
                if (current.Value.Equals(data))
                {
                    return true;
                }
                current = current.Next;
            }
            return false;
        }

        /// <summary>
        /// Метод удаляет соответствующий элемент из списка
        /// </summary>
        /// <param name="data"></param>
        public bool Remove(T data)
        {
            Node<T> current = _head;
            Node<T> previous = null;

            do
            {
                if (current.Value.Equals(data))
                {
                    if (previous == null)
                    {
                        _head = _head.Next;
                        _tail.Next = _head;
                    }
                    else
                    {
                        if (_tail == current)
                        {
                            _tail = previous;
                        }
                        previous.Next = current.Next;
                    }
                    Count--;
                    return true;
                }
                previous = current;
                current = current.Next;
            }
            while (current != _head);
            return false;
        }

        public IEnumerator GetEnumerator()
        {
            Node<T> current = _head;
            do
            {
                if (current != null)
                {
                    yield return current;
                    current = current.Next;
                }
            }
            while (current != _head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        /// <summary>
        /// Метод сортирует элементы коллекции
        /// </summary>
        public void Sort()
        {
            Node<T> temp;
            Node<T>[] mas = this.ToArray();
            Node<T>[] unsortedmas = new Node<T>[Count];
            mas.CopyTo(unsortedmas, 0);    

            // Сортируем пузырьком
            for (int i = 0; i < mas.Length; i++)
            {
                for (int j = 0; j < mas.Length - 1; j++)
                {
                    if (mas[j] > mas[j + 1])
                    {
                        temp = mas[j + 1];
                        mas[j + 1] = mas[j];
                        mas[j] = temp;
                    }
                }
            }

            // Переопределяем ссылки на элементы массива
            for (int i = 0; i < Count; i++)
            {
                unsortedmas[i] = mas[i];

                if (i < Count - 1)
                {
                    unsortedmas[i].Next = mas[i + 1];
                }
            }
            _head = mas[0];
            _tail = mas[Count - 1];
            _tail.Next = mas[0];

            _isSorted = true;
        }

        /// <summary>
        /// Метод двоичного поиска значения в списке.
        /// </summary>
        /// <param name="searchedValue">Искомое значение.</param>
        /// <returns>Индекс элемента коллекции.</returns>
        public int BinarySearch(T searchedValue)
        {
            int middle;
            int left = 0;
            int right = this.Count;

            if (!_isSorted)
            {
                throw new Exception("Коллекция не отсортирована. Для поиска в коллекции ее элементы должны быть отсортированы!");
            }

            while (left < right)
            {
                middle = left + (right - left) / 2;

                if (this[middle].Value.CompareTo(searchedValue) > 0)
                {
                    right = middle - 1;
                }
                else if (this[middle].Value.CompareTo(searchedValue) < 0)
                {
                    left = middle + 1;
                }
                else if (this[middle].Value.Equals(searchedValue))
                {
                    return middle;
                }
            }
            return -1;
        }
    }
}
