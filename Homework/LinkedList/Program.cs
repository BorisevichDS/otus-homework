﻿ using System;

namespace LinkedList
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                LinkedCircleList<int> cl = new LinkedCircleList<int>();

                cl.Add(6);
                cl.Add(3);
                cl.Add(192);
                cl.Add(9);
                cl.Add(5);
                cl.Add(18);

                Console.WriteLine("Полный список элементов");
                foreach (var node in cl)
                {
                    Console.WriteLine(node);
                }

                cl.Remove(9);

                Console.WriteLine();
                Console.WriteLine("Список элментов после удаления");
                foreach (var node in cl)
                {
                    Console.WriteLine(node);
                }

                Console.WriteLine();
                Console.WriteLine("Отсортированный список");
                cl.Sort();
                foreach (var n in cl)
                {
                    Console.WriteLine(n);
                }

                Console.WriteLine();
                Console.WriteLine("Демонстрация бинарного поиска");
                int searchedValue = 6;
                Console.WriteLine($"Индекс элемента коллекции со значением {searchedValue} равен {cl.BinarySearch(searchedValue)}");

            }
            catch (Exception e)
            {
                Console.WriteLine($"Ошибка!");
                Console.WriteLine($"Описание: {e.Message}");
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
