﻿using System;
using System.Collections.Generic;

namespace LinkedList
{
    /// <summary>
    /// Класс, описывает ноду
    /// </summary>
    internal class Node<T> where T: IComparable
    {
        public T Value { get; }
        public Node<T> Next { get; set; }
        public Node(T data)
        {
            Value = data;
        }

        public override string ToString()
        {
            return Value.ToString();
        }

        public static bool operator <(Node<T> value1, Node<T> value2)
        {
            if (value1.Value.CompareTo(value2.Value) < 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool operator >(Node<T> value1, Node<T> value2)
        {
            if (value1.Value.CompareTo(value2.Value) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
