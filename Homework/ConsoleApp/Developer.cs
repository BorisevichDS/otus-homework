﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// Разработчик
    /// </summary>
    internal class Developer : Human
    {
        /// <summary>
        /// Список знаний разработчика
        /// </summary>
        public List<Knowledge> Knowledges { get; set; }

        /// <summary>
        /// Рейтинг
        /// </summary>
        public Rating Rating { get; set; } 

        public Developer(string firstname, string lastname, Gender gender) : base(firstname, lastname, gender)
        {
            Knowledges = new List<Knowledge>();
        }

        public override string ToString() => FirstName + " " + LastName;
    }

    /// <summary>
    /// Виды специализации.
    /// </summary>
    public enum Specialization
    {
        SQLServer,
        DotNet,
        FullStack
    }

    /// <summary>
    /// Виды грейдой (уровень знаний).
    /// </summary>
    public enum Grade
    {
        Junior,
        Middle,
        Senior
    }

    /// <summary>
    /// Структура описывает знания человека
    /// </summary>
    public struct Knowledge
    {
        public Specialization Specialization { get; private set; }
        public Grade Grade { get; private set; }

        public Knowledge(Specialization s, Grade g)
        {
            Specialization = s;
            Grade = g;
        }

        public override string ToString() => $"Специализация: {Specialization}, уровень: {Grade}";
    }
}
