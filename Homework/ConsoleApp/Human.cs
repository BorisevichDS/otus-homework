﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{

    /// <summary>
    /// Базовый класс, описывающий человека
    /// </summary>
    abstract internal class Human
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; }

        /// <summary>
        /// Пол
        /// </summary>
        public Gender Gender { get; }

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="firstname">Имя</param>
        /// <param name="lastname">Фамилия</param>
        public Human(string firstname, string lastname, Gender gender)
        {
            FirstName = firstname;
            LastName = lastname;
            Gender = gender;
        }
    }

    /// <summary>
    /// Пол
    /// </summary>
    enum Gender
    {
        Male,
        Female
    }
}
