﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Определяем параметры разработчика
                Developer dev1 = new Developer("Dmitry", "Borisevich", Gender.Male);
                dev1.Knowledges.Add(new Knowledge(Specialization.DotNet, Grade.Middle));
                dev1.Knowledges.Add(new Knowledge(Specialization.SQLServer, Grade.Senior));
                dev1.Rating = new Rating(1);

                // Выводи в консоль
                ShowDeveloperInfo(dev1);

                Console.WriteLine();
                // Увеличиваем рейтинг
                dev1.Rating = dev1.Rating + 5;

                // Выводим на консоль
                ShowDeveloperInfo(dev1);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Метод показывает основную информацию о разработчике
        /// </summary>
        /// <param name="d"></param>
        static void ShowDeveloperInfo(Developer d)
        {
            Console.WriteLine($"Разработчик: {d.ToString()}");
            Console.WriteLine($"Рейтинг: {d.Rating.Value} ({d.Rating.Text})");
            foreach (Knowledge k in d.Knowledges)
            {
                Console.WriteLine(k.ToString());
            }
        }
    }
}
