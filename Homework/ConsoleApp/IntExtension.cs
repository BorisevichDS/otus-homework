﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{

    internal static class IntExtension
    {
        /// <summary>
        /// Метод переводит число в его описание 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToHowToSpell(this int val)
        {
            string result = default;
            switch (val)
            {
                case 1:
                    result = "Один";
                    break;
                case 2:
                    result = "Два";
                    break;
                case 3:
                    result = "Три";
                    break;
                case 4:
                    result = "Четрые";
                    break;
                case 5:
                    result = "Четрые";
                    break;
                default:
                    result = "Не определено";
                    break;
            }
            return result;
        }
    }
}
