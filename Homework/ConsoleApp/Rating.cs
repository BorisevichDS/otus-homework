﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// Класс описывает рейтинг
    /// </summary>
    class Rating
    {
        /// <summary>
        /// Числовое представление рейтинга
        /// </summary>
        public int Value { get; }

        /// <summary>
        /// Текстовое представление рейтинга
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="val">Числовое представление рейтинга</param>
        public Rating(int val)
        {
            if (val >= 1 && val <= 5)
            {
                Value = val;
                Text = val.ToHowToSpell();
            }
            else
                throw new ArgumentOutOfRangeException(nameof(val), $"Параметр {nameof(val)} может находиться в диапазон от 1 до 10!");
        }

        public static Rating operator +(Rating r1, Rating r2) => new Rating((r1.Value + r2.Value) / 2);
        public static Rating operator +(Rating r1, int k) => new Rating((r1.Value + k) / 2);

    }
}
