﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace RegExp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string html;
                string pattern;

                using (WebClient client = new WebClient())
                {
                    html = client.DownloadString("https://habr.com/en/top/");
                }
                pattern = @"(?<2>href|content|src)=""(?<1>http[^""]+)[^.js]""";
                MatchCollection m = Regex.Matches(html, pattern, RegexOptions.IgnoreCase);

                for (int i = 0; i < m.Count; i++)
                {
                    Console.WriteLine($"Index = {m[i].Index}, value = {m[i].Groups[1]}");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
