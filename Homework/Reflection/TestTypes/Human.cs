﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reflection
{
    /// <summary>
    /// Базовый класс, описывающий человека
    /// </summary>
    internal class Human
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
    }
}
