﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Reflection
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string input = string.Empty;
                int iterationCount = 10_000;
                Stopwatch sw = new Stopwatch();
                string delimiter = new string('=', 10);

                Human serializedObject = new Human { FirstName = "Dmitry", LastName = "Borisevich" };
                Human deserializedObject = new Human();

                #region Рефлексия
                sw.Start();

                for (int i = 0; i < iterationCount; i++)
                {
                    input = serializedObject.Serialize();
                }
                sw.Stop();

                Console.WriteLine(delimiter + " Сериализация " + delimiter);
                Console.WriteLine($"Количество итераций: {iterationCount}, затраченное время, мс: {sw.ElapsedMilliseconds}");
                Console.WriteLine(input);

                sw.Reset();
                sw.Start();               
                for (int i = 0; i < iterationCount; i++)
                {
                    deserializedObject = SerializationDemo.Deserialize<Human>(input);
                }
                sw.Stop();

                Console.WriteLine(delimiter + " Десериализация " + delimiter);
                Console.WriteLine($"Количество итераций: {iterationCount}, затраченное время, мс: {sw.ElapsedMilliseconds}");
                Console.WriteLine($"FirstName = {deserializedObject.FirstName}, LastName = {deserializedObject.LastName}");
                #endregion

                #region NewtonsoftJSON

                sw.Reset();
                sw.Start();
                for (int i = 0; i < iterationCount; i++)
                {
                    input = JsonConvert.SerializeObject(serializedObject);
                }
                sw.Stop();

                Console.WriteLine(delimiter + " Сериализация NewtonsoftJSON" + delimiter);
                Console.WriteLine($"Количество итераций: {iterationCount}, затраченное время, мс: {sw.ElapsedMilliseconds}");
                Console.WriteLine(input);

                object deserializedObject2 = new object();
                sw.Reset();
                sw.Start();
                for (int i = 0; i < iterationCount; i++)
                {
                    deserializedObject2 = JsonConvert.DeserializeObject(input, typeof(Human));
                }
                sw.Stop();

                deserializedObject = deserializedObject2 as Human;
                Console.WriteLine(delimiter + " Десериализация NewtonsoftJSON" + delimiter);
                Console.WriteLine($"Количество итераций: {iterationCount}, затраченное время, мс: {sw.ElapsedMilliseconds}");
                Console.WriteLine($"FirstName = {deserializedObject.FirstName}, LastName = {deserializedObject.LastName}");
                #endregion
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
