﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Reflection
{

    internal static class SerializationDemo
    {
        /// <summary>
        /// Метод сериализует объект в строку.
        /// </summary>
        /// <param name="type">Тип, для которого проводится сериализация.</param>
        /// <returns></returns>
        public static string Serialize(this object obj)
        {
            string ret = string.Empty;

            ret += SerializeFields(obj);
            ret += SerializeProperties(obj);

            return ret.Trim();
        }

        /// <summary>
        /// Метод сериализует свойства объекта в строку.
        /// </summary>
        /// <param name="obj">Сериализуемый объект.</param>
        /// <returns></returns>
        private static string SerializeProperties(object obj)
        {
            string ret = string.Empty;
            Type t = obj.GetType();

            PropertyInfo[] propertyInfo = t.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).OrderBy(f => f.Name).ToArray<PropertyInfo>();

            foreach (PropertyInfo pi in propertyInfo)
            {
                ret += $"isField = false, isProperty = true, isPrivate = false, type = {pi.PropertyType}, name = {pi.Name}, value = {pi.GetValue(obj)};" + Environment.NewLine;
            }
            return ret;
        }

        /// <summary>
        /// Метод сериализует поля объекта в строку.
        /// </summary>
        /// <param name="obj">Сериализуемый объект.</param>
        /// <returns></returns>
        private static string SerializeFields(object obj)
        {
            string ret = string.Empty;
            Type t = obj.GetType();

            FieldInfo[] fieldInfo = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).OrderBy(f => f.Name).ToArray<FieldInfo>();

            foreach (FieldInfo fi in fieldInfo)
            {
                ret += $"isField = true, isProperty = false, isPrivate = {fi.IsPrivate}, type = {fi.FieldType}, name = {fi.Name}, value = {fi.GetValue(obj)};" + Environment.NewLine;
            }
            return ret;
        }

        public static T Deserialize<T>(string inputString) where T : new()
        {
            // Создаем объект
            T ret = new T();

            foreach (var r in inputString.Split(";" + Environment.NewLine))
            {
                // Парсим строку
                Dictionary<string, string> row = Parse(r);

                // Проставляем значения для полей.
                SetFieldsValue<T>(ret, row);

                // Проставляем значения для свойств.
                SetPropertiesValue<T>(ret, row);
            }
            return ret;
        }

        /// <summary>
        /// Метод парсит входную строку.
        /// </summary>
        /// <param name="inputString">Входная строка.</param>
        /// <returns>Возвращает словарь атрибутов.</returns>
        private static Dictionary<string, string> Parse(string inputString)
        {
            Dictionary<string, string> parsedRow = new Dictionary<string, string>();
            string[] split = inputString.Split(", ");
            string[] split2;

            foreach (string s in split)
            {
                split2 = s.Split(" = ");
                parsedRow.Add(split2[0].Trim(), split2[1].Trim());
            }
            return parsedRow;
        }

        /// <summary>
        /// Метод устанавливает значения для полей десериализуемого объекта.
        /// </summary>
        /// <typeparam name="T">Тип дересериализуемого объекта.</typeparam>
        /// <param name="obj">Десериализованный объект.</param>
        /// <param name="row">Словарь атрибутов.</param>
        private static void SetFieldsValue<T>(T obj, Dictionary<string, string> row) where T : new()
        {
            string name = string.Empty;
            string value = string.Empty;
            bool isField = false;

            isField = row.Where(r => r.Key.Equals("isField") && r.Value.Equals("true")).Select(r => true).FirstOrDefault();

            if (isField)
            {
                name = row.Where(r => r.Key.Equals("name")).Select(r => r.Value).FirstOrDefault();
                value = row.Where(r => r.Key.Equals("value")).Select(r => r.Value).FirstOrDefault();

                Type t = typeof(T);
                t.GetField(name)?.SetValue(obj, value);
            }
        }

        /// <summary>
        /// Метод устанавливает значения для свойств десериализуемого объекта.
        /// </summary>
        /// <typeparam name="T">Тип дересериализуемого объекта.</typeparam>
        /// <param name="obj">Десериализованный объект.</param>
        /// <param name="row">Словарь атрибутов.</param>
        private static void SetPropertiesValue<T>(T obj, Dictionary<string, string> row) where T : new()
        {
            string name = string.Empty;
            string value = string.Empty;
            bool isProperty = false;

            isProperty = row.Where(r => r.Key.Equals("isProperty") && r.Value.Equals("true")).Select(r => true).FirstOrDefault();

            if (isProperty)
            {
                name = row.Where(r => r.Key.Contains("name")).Select(r => r.Value).FirstOrDefault();
                value = row.Where(r => r.Key.Contains("value")).Select(r => r.Value).FirstOrDefault();

                Type t = typeof(T);
                t.GetProperty(name)?.SetValue(obj, value);
            }
        }
    }
}
