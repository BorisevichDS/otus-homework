﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    /// <summary>
    /// Банковский счет.
    /// </summary>
    class Account
    {
        private DateTime _openedOn;
        private double _sum = 0.0;
        private int _userId;

        /// <summary>
        /// Идентификатор счета.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Дата открытия счёта.
        /// </summary>
        public DateTime OpenedOn => _openedOn;

        /// <summary>
        /// Сумма на счете.
        /// </summary>
        public double Sum => _sum;

        /// <summary>
        /// Идентификатор владельца счета.
        /// </summary>
        public int UserId => _userId;

        public Account(int id, int userId, double startSum)
        {
            if (startSum > 0)
            {
                _sum = startSum;
            }
            else
            {
                throw new ArgumentOutOfRangeException("startSum", "Начальная сумма счета должна быть больше 0.");
            }

            _userId = userId;
            _openedOn = DateTime.Now;
            Id = id;
        }
    }
}
