﻿using System;

namespace LINQ
{
    /// <summary>
    /// Пользователь.
    /// </summary>
    class User
    {
        private DateTime _registrationDate;

        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; private set; }

        /// <summary>
        /// Отчество.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; private set; }

        /// <summary>
        /// Номер мобильного телефона.
        /// </summary>
        public string MobilePhone { get; private set; }

        /// <summary>
        /// Данные паспорта.
        /// </summary>
        public PassportData PassportInfo { get; set; }

        public DateTime RegistrationDate => _registrationDate;

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; private set; }

        /// <summary>
        /// Пароль.
        /// </summary>
        public string Password { get; private set; }

        public User(int id, string lastName, string firstName, string mobilePhone, string login, string password)
        {
            Id = id;
            LastName = lastName;
            FirstName = firstName;
            MobilePhone = mobilePhone;
            Login = login;
            Password = password;
            
            _registrationDate = DateTime.Now.Date;
            ;
        }

        public override string ToString()
        {
            return $"ID = {Id} " + Environment.NewLine +
                   $"ФИО = {LastName} {FirstName} {MiddleName}" + Environment.NewLine +
                  $"Логин = {Login}" + Environment.NewLine +
                  $"Дата регистрации = {RegistrationDate.ToShortDateString()}";
        }
    }

    /// <summary>
    /// Паспортные данные.
    /// </summary>
    struct PassportData
    {
        private int _seria;
        private int _number;

        /// <summary>
        /// Серия.
        /// </summary>
        public int Seria
        {
            get
            {
                return _seria;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("seria", "Серия паспорта не может быть отрицательной.");
                }
                else if (value.ToString().Length != 4)
                {
                    throw new ArgumentOutOfRangeException("seria", "Серия паспорта должна представлять собой 4-х значное число.");
                }
                else
                {
                    _seria = value;
                }
            }

        }

        /// <summary>
        /// Номер.
        /// </summary>
        public int Number
        {
            get
            {
                return _number;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("number", "Номер паспорта не может быть отрицательным.");
                }
                else if (value.ToString().Length != 6)
                {
                    throw new ArgumentOutOfRangeException("number", "Номер паспорта должен представлять собой 6-ти значное число.");
                }
                else
                {
                    _number = value;
                }
            }
        }
    }
}
