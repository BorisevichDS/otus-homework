﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    // Id, дата операции, тип операции, сумма, Id счёта     Каждая запись в истории счвязана с конкретным счёто
    
    /// <summary>
    /// История операций со счётом.
    /// </summary>
    class History
    {
        private static int _currentid;
        private int _id;
        private DateTime _operationDateTime;

        /// <summary>
        /// Идентификатор операции.
        /// </summary>
        public int Id => _id;

        /// <summary>
        /// Дата и время операции.
        /// </summary>
        public DateTime OperationDateTime => _operationDateTime;
        
        /// <summary>
        /// Тип операции.
        /// </summary>
        public OperationType Type { get; private set; }

        /// <summary>
        /// Сумма операции.
        /// </summary>
        public double Sum { get; private set; }

        /// <summary>
        /// Идентификатор счёта.
        /// </summary>
        public int AccountId { get; private set; }

        public History(OperationType type, double sum, int accountId)
        {
            Type = type;
            Sum = sum;
            AccountId = accountId;
            _operationDateTime = DateTime.Now;
            _id = _currentid++;
        }

        /// <summary>
        /// Тип операций.
        /// </summary>
        public enum OperationType
        {
            /// <summary>
            /// Начисление
            /// </summary>
            Add,
            /// <summary>
            /// Списание
            /// </summary>
            WriteOff
        }
    }
}
