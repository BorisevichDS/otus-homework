﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    /// <summary>
    /// Счета пользователя. 
    /// </summary>
    internal class UserAccount
    {
        /// <summary>
        /// Фамилия владельца счета.
        /// </summary>
        public string UserLastName { get; }

        /// <summary>
        /// Отчество владельца счета.
        /// </summary>
        public string UserMiddleName { get; }

        /// <summary>
        /// Имя владельца счета.
        /// </summary>
        public string UserFirstName { get; }

        /// <summary>
        /// Идентификатор счета.
        /// </summary>
        public int AccountID { get; }

        /// <summary>
        /// Баланс счета.
        /// </summary>
        public double AccountSum { get; }

        /// <summary>
        /// Дата и время открытия счета.
        /// </summary>
        public DateTime AccountOpenedOn { get; }

        public UserAccount(string userLastName, string userMiddleName, string userFirstName, int accountID)
        {
            UserLastName = userLastName;
            UserMiddleName = userMiddleName;
            UserFirstName = userFirstName;
            AccountID = accountID;
        }

        public UserAccount(string userLastName, string userMiddleName, string userFirstName, int accountID, double accountSum, DateTime accountOpenedOn)
        {
            UserLastName = userLastName;
            UserMiddleName = userMiddleName;
            UserFirstName = userFirstName;
            AccountID = accountID;
            AccountSum = accountSum;
            AccountOpenedOn = accountOpenedOn;
        }

        public override string ToString()
        {
            return $"ФИО владельца счета: {UserLastName} {UserMiddleName} {UserFirstName}, ID счета: {AccountID}, " +
                   $"Баланс счета: {AccountSum}, Дата открытия счета: {AccountOpenedOn}";
        }

    }
}