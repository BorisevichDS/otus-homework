﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    /// <summary>
    /// Операции по счетам пользователя.
    /// </summary>
    internal class UserAccountHistory : UserAccount
    {
        /// <summary>
        /// Идентификатор операции.
        /// </summary>
        public int OperationID { get; }

        /// <summary>
        /// Дата и время операции.
        /// </summary>
        public DateTime OperationDateTime { get; }

        /// <summary>
        /// Тип операции.
        /// </summary>
        public History.OperationType OperationType { get; }

        /// <summary>
        /// Сумма операции.
        /// </summary>
        public double OperationSum { get; }

        public UserAccountHistory(string userLastName, string userMiddleName, string userFirstName, int accountID, int operationId, DateTime operationDateTime, History.OperationType operationType, double operationSum) : base(userLastName, userMiddleName, userFirstName, accountID)
        {
            OperationID = operationId;
            OperationDateTime = operationDateTime;
            OperationType = operationType;
            OperationSum = operationSum;
        }

        public override string ToString()
        {
            return $"ФИО владельца счета: {UserLastName} {UserMiddleName} {UserFirstName}, ID счета: {AccountID}, " +
                   $"ID операции: {OperationID}, дата операции: {OperationDateTime}, тип операции: {OperationType}, сумма: {OperationSum}";
        }
    }
}