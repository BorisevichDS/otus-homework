﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    /// <summary>
    /// Вспомогательный класс для вывода информации о всех операциях пополнения счёта с указанием владельца каждого счёта.
    /// </summary>
    internal static class UserAccountHistoryAddInfo
    {
        public static void ShowInfoByLoginAndPassword(List<User> users, List<Account> accounts, List<History> operationHistory, string searchedLogin, string searchedPassword)
        {
            List<UserAccountHistory> userAccountsHistory = UserAccountHistoryInfo.GetUserAccountHistoryByLoginAndPassword(users, accounts, operationHistory, searchedLogin, searchedPassword);
            var userAccountsHistoryFiltered = userAccountsHistory.Where(h => h.OperationType == History.OperationType.Add).ToList();

            Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта.");

            if (userAccountsHistoryFiltered.Count > 0)
            {
                Info.Print(userAccountsHistoryFiltered);
            }
            else
            {
                Console.WriteLine("Данные не найдены.");
            }

        }
    }
}
