﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    /// <summary>
    /// Вспомогательный класс для вывода информации о всех счетах пользователя с соответствующим логином и паролем.
    /// </summary>
    internal static class UserAccountInfo
    {
        /// <summary>
        /// Метод возвращает список счетов пользователей.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        /// <param name="accounts">Список всех счетов в банке.</param>
        /// <returns>Список счетов пользователей из переданного списка.</returns>
        public static List<UserAccount> GetUserAccountsByLoginAndPassword(List<User> users, List<Account> accounts, string searchedLogin, string searchedPassword)
        {
            List<User> filteredUsers = UserInfo.GetUserByLoginAndPassword(users, searchedLogin, searchedPassword);
            return accounts.Join(filteredUsers
                    , a => a.UserId
                    , u => u.Id
                    , (a, u) => new UserAccount(u.LastName, u.MiddleName, u.FirstName, a.Id, a.Sum, a.OpenedOn)).ToList();
        }

        /// <summary>
        /// Выводит в консоль информацию о счетах пользователя, с соответствующим логином и паролем.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        /// <param name="accounts">Список счетов.</param>
        /// <param name="searchedLogin">Логин пользователя, вледльца счетов.</param>
        /// <param name="searchedPassword">Пароль пользователя, владельца счетов.</param>
        public static void ShowInfoByLoginAndPassword(List<User> users, List<Account> accounts, string searchedLogin, string searchedPassword)
        {
            List<UserAccount> userAccounts = GetUserAccountsByLoginAndPassword(users, accounts, searchedLogin, searchedPassword);

            Console.WriteLine("2. Вывод данных о всех счетах заданного пользователя");

            if (userAccounts.Count > 0)
            {
                Info.Print(userAccounts);
            }
            else
            {
                Console.WriteLine("Данные не найдены.");
            }
        }
    }
}
