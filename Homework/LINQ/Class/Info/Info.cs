﻿using System;
using System.Collections;

namespace LINQ
{
    /// <summary>
    /// Базовый класс для вывода информации.
    /// </summary>
    internal class Info
    {
        /// <summary>
        /// Метод запрашивает логин и пароль для поиска пользователя.
        /// </summary>
        public static void AskLoginAndPassword(out string searchedLogin, out string searchedPassword)
        {
            Console.WriteLine("Введите логин и пароль для поиска пользователя");
            Console.Write("Login: ");
            searchedLogin = Console.ReadLine().Trim();
            Console.Write("Password: ");
            searchedPassword = Console.ReadLine().Trim();
        }

        /// <summary>
        /// Распечатывает список или массив в консоль.
        /// </summary>
        /// <param name="list"></param>
        public static void Print(IEnumerable list)
        {
            foreach (var el in list)
            {
                Console.WriteLine(el);
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Метод запрашивает сумму для фильтрации счетов.
        /// </summary>
        /// <returns></returns>
        public static double AskInputSum()
        {
            Console.Write("Введите N: ");
            string inputN = Console.ReadLine();
            if (!double.TryParse(inputN, out double inputSum))
            {
                throw new ArgumentOutOfRangeException("N", "Сумма задана не верно!");
            }
            return inputSum;
        }
    }
}
