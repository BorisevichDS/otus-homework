﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    /// <summary>
    /// Вспомогательный класс для вывода информации о пользователе с соответствующим логином и паролем.
    /// </summary>
    internal class UserInfo
    {
        /// <summary>
        /// Метод возвращает список пользователей c логином и паролем, соответствующим переданным в параметрах searchedLogin и searchedPassword.
        /// </summary>
        /// <param name="users">Список пользователей для поиска.</param>
        /// <param name="searchedLogin">Логин.</param>
        /// <param name="searchedPassword">Пароль.</param>
        /// <returns>Список пользователей с соответствующим логином и паролем.</returns>
        public static List<User> GetUserByLoginAndPassword(List<User> users, string login, string password)
        {
            return users.Where(u => u.Login.Equals(login) && u.Password.Equals(password)).ToList();
        }

        /// <summary>
        /// Выводит в консоль информацию о пользователе по логину и паролю.
        /// </summary>
        /// <param name="users">Список пользователей для поиска.</param>
        /// <param name="login">Логин.</param>
        /// <param name="password">Пароль.</param>
        public static void ShowInfoByLoginAndPassword(List<User> users, string login, string password)
        {
            List<User> filteredUsers = GetUserByLoginAndPassword(users, login, password);

            Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю.");

            if (filteredUsers.Count > 0)
            {
                Info.Print(filteredUsers);
            }
            else
            {
                Console.WriteLine("Пользователь не найден.");
            }
        }

        /// <summary>
        /// Выводит в консоль информацию о всех пользователях, у которых на счёте сумма больше переданной.
        /// </summary>
        /// <param name="users">Список пользователей для поиска.</param>
        /// <param name="accounts">Список счетов для поиска.</param>
        /// <param name="inputSum">Сумма.</param>
        public static void ShowInfoByLoginAndPassword(List<User> users, List<Account> accounts, double inputSum)
        {
            var usersFilteredByInputValue = accounts.Where(a => a.Sum > inputSum).Join(users,
                a => a.UserId,
                u => u.Id,
                (a, u) => new { u.LastName, u.FirstName, AccountId = a.Id, AccountSum = a.Sum }).ToList();

            Console.WriteLine("5. Вывод данных о всех пользователях, у которых на счёте сумма больше N(N задаётся из вне и может быть любой).");

            if (usersFilteredByInputValue.Count > 0)
            {
                Info.Print(usersFilteredByInputValue);
            }
            else
            {
                Console.WriteLine("Данные не найдены.");
            }
        }
    }
}