﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    /// <summary>
    /// Вспомогательный класс для вывода информации о всех счетах заданного пользователя, включая историю по каждому счёту.
    /// </summary>
    internal static class UserAccountHistoryInfo
    {

        /// <summary>
        /// Метод возвращает список операций по счетам пользователя.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        /// <param name="accounts">Список счетов.</param>
        /// <param name="operationHistory">Список операций по счетам.</param>
        /// <param name="searchedLogin">Логин пользователя, для которого возвращается список операций.</param>
        /// <param name="searchedPassword">Пароль пользователя, для которого возвращается список операций.</param>
        /// <returns></returns>
        public static List<UserAccountHistory> GetUserAccountHistoryByLoginAndPassword(List<User> users, List<Account> accounts, List<History> operationHistory, string searchedLogin, string searchedPassword)
        {
            List<UserAccount> userAccounts = UserAccountInfo.GetUserAccountsByLoginAndPassword(users, accounts, searchedLogin, searchedPassword);

            var userAccountsHistory = from a in userAccounts
                                      join o in operationHistory
                                      on a.AccountID equals o.AccountId
                                      select new UserAccountHistory(a.UserLastName, a.UserMiddleName, a.UserFirstName, a.AccountID, o.Id, o.OperationDateTime, o.Type, o.Sum);

            return userAccountsHistory.ToList();
        }

        /// <summary>
        /// Выводит в консоль информацию об операциях по счетам пользователя, с соответствующим логином и паролем.
        /// </summary>
        /// <param name="users">Список пользователей.</param>
        /// <param name="accounts">Список счетов.</param>
        /// <param name="operationHistory">Список операций по счетам.</param>
        /// <param name="searchedLogin">Логин пользователя, для которого возвращается список операций.</param>
        /// <param name="searchedPassword">Пароль пользователя, для которого возвращается список операций.</param>
        public static void ShowInfoByLoginAndPassword(List<User> users, List<Account> accounts, List<History> operationHistory, string searchedLogin, string searchedPassword)
        {
            List<UserAccountHistory> userAccountsHistory = GetUserAccountHistoryByLoginAndPassword(users, accounts, operationHistory, searchedLogin, searchedPassword);

            Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.");

            if (userAccountsHistory.Count > 0)
            {
                Info.Print(userAccountsHistory);
            }
            else
            {
                Console.WriteLine("Данные не найдены.");
            }
        }

    }
}
