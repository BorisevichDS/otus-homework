﻿using System;
using System.Collections.Generic;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                #region Intialization
                List<User> users = new List<User>()
                {
                    new User(1, "Безос", "Джефф", "+7(926) 000 00 01", "Bezos", "SomePassword01"),
                    new User(2, "Гейтс", "Билл", "+7(926) 000 00 02", "Gates", "SomePassword02"),
                    new User(3, "Баффет", "Уорен", "+7(926) 000 00 03", "Baffet", "SomePassword03"),
                };

                List<Account> accounts = new List<Account>()
                {
                    // Безос
                    new Account(1, 1, 131000.0),
                    // Гейтс
                    new Account(2, 2, 965000.0),
                    new Account(3, 2, 300000.0),

                    // Баффет
                    new Account(4, 3, 825000.0)
                };

                List<History> operationHistory = new List<History>()
                {
                    // Безос
                    new History(History.OperationType.Add, 10000.0, 1),
                    new History(History.OperationType.Add, 20000.0, 1),
                    new History(History.OperationType.Add, 5000.0, 1),
                    new History(History.OperationType.WriteOff, 100.0, 1),
                    new History(History.OperationType.WriteOff, 5000.0, 1),

                    // Гейтс
                    new History(History.OperationType.Add, 5000000.0, 2),
                    new History(History.OperationType.Add, 2500.0, 3),
                    new History(History.OperationType.Add, 500.0, 3),
                    new History(History.OperationType.WriteOff, 1000.0, 3),

                    // Баффет
                    new History(History.OperationType.Add, 20000000.0, 4),
                    new History(History.OperationType.WriteOff, 5000.0, 4),
                    new History(History.OperationType.WriteOff, 25000.0, 4),
                    new History(History.OperationType.WriteOff, 35000.0, 4)
                };

                Dictionary<int, string> optionList = new Dictionary<int, string>();
                optionList.Add(1, "Вывод информации о заданном аккаунте по логину и паролю");
                optionList.Add(2, "Вывод данных о всех счетах заданного пользователя");
                optionList.Add(3, "Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
                optionList.Add(4, "Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");
                optionList.Add(5, "Вывод данных о всех пользователях, у которых на счёте сумма больше N(N задаётся из вне и может быть любой)");
                #endregion

                char inputString;

                do
                {
                    Console.WriteLine("Варианты решения:");
                    Info.Print(optionList);
                    Console.Write("Выберите вариант решения: ");
                    string inputOption = Console.ReadLine().Trim();

                    if (int.TryParse(inputOption, out int option) && optionList.ContainsKey(option))
                    {
                        string searchedLogin;
                        string searchedPassword;

                        switch (option)
                        {
                            // 1. Вывод информации о заданном аккаунте по логину и паролю.
                            case 1:
                                Info.AskLoginAndPassword(out searchedLogin, out searchedPassword);
                                UserInfo.ShowInfoByLoginAndPassword(users, searchedLogin, searchedPassword);
                                break;
                            // 2. Вывод данных о всех счетах заданного пользователя.
                            case 2:
                                Info.AskLoginAndPassword(out searchedLogin, out searchedPassword);
                                UserAccountInfo.ShowInfoByLoginAndPassword(users, accounts, searchedLogin, searchedPassword);
                                break;
                            // 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.
                            case 3:
                                Info.AskLoginAndPassword(out searchedLogin, out searchedPassword);
                                UserAccountHistoryInfo.ShowInfoByLoginAndPassword(users, accounts, operationHistory, searchedLogin, searchedPassword);
                                break;
                            // 4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта.
                            case 4:
                                Info.AskLoginAndPassword(out searchedLogin, out searchedPassword);
                                UserAccountHistoryAddInfo.ShowInfoByLoginAndPassword(users, accounts, operationHistory, searchedLogin, searchedPassword);
                                break;
                            // 5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой).
                            case 5:
                                double inputSum = Info.AskInputSum();
                                UserInfo.ShowInfoByLoginAndPassword(users, accounts, inputSum);
                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Указанное решение не найдено.");
                    }
                    Console.WriteLine();
                    Console.WriteLine("Хотите повторить запрос? (Y - Да, N - Нет)?");
                    char.TryParse(Console.ReadLine().Trim().ToUpper(), out inputString);
                }
                while (inputString == 'Y');
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
